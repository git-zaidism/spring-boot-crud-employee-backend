package jahid.code.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jahid.code.exception.ResourceNotFoundException;
import jahid.code.model.Employee;
import jahid.code.repository.EmployeeRepository;

@RestController
@RequestMapping("/api/v1/")
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;

    private final Logger LOGGGER = LoggerFactory.getLogger(EmployeeController.class);


    //common method to get employee by id
    private Employee getEmployeeCommonMethod(long id) throws ResourceNotFoundException {
        Employee employee = employeeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Employee does not Exist !"));
        return employee;
    }

    // Rest API to get all Employee List
    @GetMapping("/employees") // when we hit this URL: /api/v1/employees below rest api will get called
    @CrossOrigin(origins = "http://localhost:4200")
    public List<Employee> getAllEmployees() {
        LOGGGER.info("Inside getAllEmployees()");
        return employeeRepository.findAll();
    }

    // create Employee rest Api
    @PostMapping("/employees")
    @CrossOrigin(origins = "http://localhost:4200")
    public Employee createEmployee(@Valid @RequestBody Employee employee) {
        LOGGGER.info("Inside createEmployee()");
        return employeeRepository.save(employee);
    }

    // get employee by id
    @GetMapping("/employees/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable long id) {
        LOGGGER.info("Inside getEmployeeById()");
        Employee employee = getEmployeeCommonMethod(id);
        return ResponseEntity.ok(employee);
    }

    // update employee rest api
    @PutMapping("/employees/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Employee> updateEmployee(@PathVariable long id, @Valid @RequestBody Employee employeeDetails) {
        LOGGGER.info("Inside updateEmployee()");
        Employee employee = getEmployeeCommonMethod(id);
        employee.setFirstName(employeeDetails.getFirstName());
        employee.setLastName(employeeDetails.getLastName());
        employee.setEmailId(employeeDetails.getEmailId());
        Employee Updatedemployee = employeeRepository.save(employee);
        return ResponseEntity.ok(Updatedemployee);
    }

    // delete employee rest api
    @DeleteMapping("/employees/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Map<String, Boolean>> deleteEmployee(@PathVariable long id) {
        LOGGGER.info("Inside deleteEmployee()");
        Employee employee = getEmployeeCommonMethod(id);
        employeeRepository.delete(employee);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", true);
        return ResponseEntity.ok(response);
    }
}