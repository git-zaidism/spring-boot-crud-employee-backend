/*JPA Repository provides us CRUD methods for database without writing them */
package jahid.code.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jahid.code.model.*;

@Repository // long is primary key data type and Employee is model class. Open implementation of JpaRepository to check ready made method written in JpaRepository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
