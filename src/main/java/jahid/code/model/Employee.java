package jahid.code.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;

/*This is Employee JPA Entity*/
//JPA annotation in model to map model class to the relational database table

@Entity
@Table(name = "employees")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // for making id as primary key
    private long id;

    //column name using @column
    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email_id")
    @Email(message = "E-mail Id is not Valid")
    private String emailId;


}
