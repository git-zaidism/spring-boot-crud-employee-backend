package jahid.code;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.CrossOrigin;

@SpringBootApplication
public class SpringbootBackedApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootBackedApplication.class, args);
		}
}
